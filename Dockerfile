FROM debian:buster
RUN set -x &&\
apt update ;\
apt install gnupg1 apt-transport-https dirmngr ca-certificates -y && \
export INSTALL_KEY=379CE192D401AB61 &&\
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys $INSTALL_KEY && \
echo "deb https://ookla.bintray.com/debian buster main" | tee  /etc/apt/sources.list.d/speedtest.list && \
apt update && \
apt install speedtest &&\
apt autoclean &&\
find /var/lib/apt/lists/ -maxdepth 1 -type f -print0 | xargs -0 rm

ENTRYPOINT ["/usr/bin/speedtest"]